import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;

public class UserRepository {
    private HashMap<Long,User> users = new HashMap<>();


    public User findById(Long id) {
        return users.get(id);
    }

    public Long create(String name, String email, String password, LocalDate birthday) {
        User user = new User(name, email, password, birthday);
        Long id = new Date().getTime();
        users.put(id, user);
        return id;
    }

}
