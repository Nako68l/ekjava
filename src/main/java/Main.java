import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        UserRepository userRepository = new UserRepository();
        UserUtil userUtil = new UserUtil(userRepository);
        Long id = userUtil.createUser("Antony", "Toxa23A@gmail.com", "absd#123", LocalDate.of(2000,5,23));
        System.out.println(userRepository.findById(id));
    }
}
